const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  categories: [{ type: Array }], // ฟิลด์สำหรับ categories
  role: { type: String, required: true, default: 'user' }, // เพิ่มฟิลด์ role พร้อมกำหนดค่าเริ่มต้น
  active: { type: Boolean, required: true, default: true } // เพิ่มฟิลด์ active พร้อมกำหนดค่าเริ่มต้น
});

const User = mongoose.model('User', userSchema);

module.exports = User;