const mongoose = require('mongoose');

const shopSchema = new mongoose.Schema({
  name: { type: String, required: true },
  thumbnail: { type: String, required: true },
  rating: { type: Number, required: true },
  location: { type: String, required: true }, // เปลี่ยนเป็น string
  remark: { type: String }, // เพิ่ม field remark
  menu_list: [{ name: String, image: String }],
  categories: [{ type: Array }] 
});

const Shop = mongoose.model('Shop', shopSchema);

module.exports = Shop;