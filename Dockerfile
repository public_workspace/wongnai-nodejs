FROM node:21

WORKDIR /wongnai-nodejs

COPY package*.json ./

RUN npm install --force

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]
