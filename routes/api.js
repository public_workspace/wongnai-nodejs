const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../model/userModel');
const Shop = require('../model/shopModel');
const Category = require('../model/categoriesModel');

router.get('/', (req, res) => {
    res.send('wongnai-nodejs-api V1');
});


const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) return res.sendStatus(401);

    jwt.verify(token, 'your_secret_key', (err, user) => {
        if (err) return res.sendStatus(403);
        req.user = user;
        next();
    });
};


router.post('/register', async (req, res) => {
    try {
      const { email, password, categories } = req.body;
      const hashedPassword = await bcrypt.hash(password.toString(), 8);
      const user = new User({
        email,
        password: hashedPassword,
        categories
      });
      await user.save();
      res.status(201).send({ status: true, message: 'User created', user: user });
    } catch (error) {
      console.log(error);
      res.status(500).send({ status: false, message: 'Error creating user' });
    }
  });

  router.post('/login', async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.email });
        if (!user) {
            return res.status(400).send({ status: false, message: 'User not found' });
        }
        const isPasswordValid = await bcrypt.compare(req.body.password.toString(), user.password);
        if (!isPasswordValid) {
            return res.status(400).send({ status: false, message: 'Invalid password' });
        }
        const token = jwt.sign({ email: user.email }, 'your_secret_key', { expiresIn: '24h' }); // ควรจัดการ secret key ให้ปลอดภัย
        const categories = user.categories

        let flattenedArray = categories.flat();
        
        req.session.accessToken = token;
        req.session.categories = flattenedArray;
        req.session.role = user.role;

        // รวม categories ไปใน response
        res.send({ status: true, token: token, categories: user.categories , role : user.role });
    } catch (error) {
        console.error(error);
        res.status(500).send({ status: false, message: 'Login error' });
    }
});

router.get('/logout', async (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.error('Error destroying session:', err);
    } else {
      res.redirect('/login'); 
    }
  });
});

router.get('/get-user', authenticateToken,async (req, res) => {
  try {
    const users = await User.find({}, '-password'); // ดึงข้อมูลผู้ใช้ทั้งหมด แต่ไม่รวมฟิลด์ password
    res.status(200).send({ status: true, users });
  } catch (error) {
    console.error(error);
    res.status(500).send({ status: false, message: 'Error fetching users' });
  }
});

router.post('/ban-user', authenticateToken,async (req, res) => {
  try {
    const { email } = req.body; // สมมติว่ารับ email ของผู้ใช้ที่ต้องการ ban
    const result = await User.updateOne({ email: email }, { active: false });

    if (result.modifiedCount === 0) {
      return res.status(404).send({ status: false, message: 'User not found or already inactive' });
    }

    res.send({ status: true, message: 'User has been banned successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).send({ status: false, message: 'Error banning user' });
  }
});

router.post('/create-shop', authenticateToken ,async (req, res) => {
    try {
      const { name, thumbnail, rating, location, menu_list, remark , categories } = req.body;
      const shop = new Shop({
        name,
        thumbnail,
        rating,
        location,
        menu_list,
        remark ,
        categories
      });
      await shop.save();
      res.status(201).send({ status: true, message: 'Shop created', shop: shop });
    } catch (error) {
      console.log(error);
      res.status(500).send({ status: false, message: 'Error creating shop' });
    }
  });

  router.post('/get-shop', authenticateToken ,async (req, res) => {
    try {


      const { categories } = req.body;

      console.log(categories);

      const allShops = await Shop.find({});
  
      const recommend_list = allShops.filter(shop => {
        // แปลง categories ของแต่ละร้านเป็น array ราบเดียว
        let shopCategories = shop.categories.flat();
        // ตรวจสอบว่าทุก elements ใน categories request มีอยู่ใน shopCategories
        return categories.some(category => shopCategories.includes(category));
      });
  
      const result_list = allShops.filter(shop => !recommend_list.includes(shop));
  
      res.status(200).send({status: true ,recommend_list, result_list });
    } catch (error) {
      console.error(error);
      res.status(500).send({ status: false,message: 'Error fetching shops' });
    }
  });

  router.post('/get-shop-by-name',authenticateToken, async (req, res) => {
    try {
      const { name } = req.body; // รับค่า name จาก body ของ request
      const regex = new RegExp(name, 'i'); // สร้าง regular expression เพื่อค้นหาชื่อที่คล้ายกัน, 'i' หมายถึง case insensitive
      const shops = await Shop.find({ name: regex });
      res.status(200).send(shops);
    } catch (error) {
      console.error(error);
      res.status(500).send({ message: 'Error fetching shop' });
    }
  });

  router.post('/insert-categories', authenticateToken ,async (req, res) => {
    try {
      const { name } = req.body;
      const category = new Category({ name });
      await category.save();
      res.status(201).send({ status: true, message: 'Category inserted', category });
    } catch (error) {
      console.log(error);
      res.status(500).send({ status: false, message: 'Error inserting category' });
    }
  });

  router.get('/get-categories' ,async (req, res) => {
    try {
      const categories = await Category.find({});
      res.status(200).send(categories);
    } catch (error) {
      console.log(error);
      res.status(500).send({ status: false, message: 'Error fetching categories' });
    }
  });

  router.post('/get-shop-by-id', authenticateToken,async (req, res) => {
    try {
      const { id } = req.body;
      const shop = await Shop.findById(id);
      if (!shop) {
        return res.status(404).send({ status: false, message: 'Shop not found' });
      }
      res.status(200).send({ status: true, shop });
    } catch (error) {
      console.error(error);
      res.status(500).send({ status: false, message: 'Error fetching shop' });
    }
  });
  

module.exports = router;