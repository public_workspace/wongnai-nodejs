const express = require('express');

const router = express.Router();


const checkAccessToken = (req, res, next) => {
    // Check if access token exists in session
    if (!req.session.accessToken) {
        // Redirect to login page
        return res.redirect('/login');
    }

    // Token exists, move to next middleware or route handler
    next();
};

const checkAdminRole = (req, res, next) => {
    if (req.session.role !== 'admin') {
        return res.redirect('/store');
    }
    next();
};

router.get('/', (req, res) => {
    res.redirect('/login');
});

router.get('/login', (req, res) => {
    res.render('login-register');
});
router.get('/about',checkAccessToken, (req, res) => {
    res.render('about' , { currentRoute: '/about' , accessToken: req.session.accessToken , role :  req.session.role });
});
router.get('/store',checkAccessToken, (req, res) => {
    res.render('store', { currentRoute: '/store' , accessToken: req.session.accessToken , categories: req.session.categories , role :  req.session.role  });
});
router.get('/store/:id',checkAccessToken, (req, res) => {
    const id = req.params.id;
    res.render('store-detail' , { currentRoute: '/store' , accessToken: req.session.accessToken , id : id , role :  req.session.role });
});

router.get('/admin-users',checkAccessToken,checkAdminRole, (req, res) => {
    res.render('admin-users', { currentRoute: '/admin-users' , accessToken: req.session.accessToken , role :  req.session.role });
});


module.exports = router;