const express = require('express');
const session = require('express-session');
const app = express();
const cors = require('cors')
const routes = require('./routes');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.use(express.urlencoded({ extended: false }));
app.use(express.static('public'));

app.use(session({
    secret: 'your-secret-key',
    resave: false,
    saveUninitialized: true
}));

app.use('/', routes);

const PORT = process.env.PORT || 3000;

const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://easytoa789:vQNd55utZkiFH1KP@cluster0.rzyhswx.mongodb.net/', { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log('Connected to MongoDB Atlas'))
  .catch(err => console.error('Could not connect to MongoDB Atlas', err));

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});